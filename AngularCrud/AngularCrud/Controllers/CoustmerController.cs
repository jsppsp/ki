﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AngularCrud.Models;
namespace AngularCrud.Controllers
{
    public class CoustmerController : Controller
    {
        dntrjatinderEntities obj = new dntrjatinderEntities();
        // GET: Coustmer
        public ActionResult Index()
        {
            return View();
        }
    public JsonResult GetAllCustomer()
        {
        List<TbCustmer> Cust = obj.TbCustmers.ToList();
        return Json(Cust,JsonRequestBehavior.AllowGet);
        }
    public JsonResult Get_CustomerById(string Id)
    {
        using (dntrjatinderEntities Obj = new dntrjatinderEntities())
        {
            int custId = int.Parse(Id);
            return Json(Obj.TbCustmers.Find(custId), JsonRequestBehavior.AllowGet);
        }
    }
    public string Insert_Custmer(TbCustmer Custmer)
    {
        if (Custmer != null)
        {
            using (dntrjatinderEntities Obj = new dntrjatinderEntities())
            {
                Obj.TbCustmers.Add(Custmer);
                Obj.SaveChanges();
                return "Custmer Added Successfully";
            }
        }
        else
        {
            return "Employee Not Inserted! Try Again";
        }
    }
    /// <summary>  
    /// Delete Employee Information  
    /// </summary>  
    /// <param name="Emp"></param>  
    /// <returns></returns>  
    public string Delete_Custmer(TbCustmer cust)
    {
        if (cust != null)
        {
            using (dntrjatinderEntities Obj = new dntrjatinderEntities())
            {
                var cust_ = Obj.Entry(cust);
                if (cust_.State == System.Data.Entity.EntityState.Detached)
                {
                    Obj.TbCustmers.Attach(cust);
                    Obj.TbCustmers.Remove(cust);
                }
                Obj.SaveChanges();
                return "Custmer Deleted Successfully";
            }
        }
        else
        {
            return "Custmer Not Deleted! Try Again";
        }
    }
    /// <summary>  
    /// Update Employee Information  
    /// </summary>  
    /// <param name="Emp"></param>  
    /// <returns></returns>  
    public string Update_Custmer(TbCustmer cust)
    {
        if (cust != null)
        {
            using (dntrjatinderEntities Obj = new dntrjatinderEntities())
            {
                var cust_ = Obj.Entry(cust);
                TbCustmer CustObj = Obj.TbCustmers.Where(x => x.CustmerId == cust.CustmerId).FirstOrDefault();
                CustObj.CustmerName = cust.CustmerName;
                CustObj.ContectNumber= cust.ContectNumber;
                CustObj.Country= cust.Country;
                Obj.SaveChanges();
                return "Custmer Updated Successfully";
            }
        }
        else
        {
            return "Custmer Not Updated! Try Again";
        }
    }  
    }
}